    /*
    1. Запитайте у користувача два числа.
    Перевірте, чи є кожне з введених значень числом.
    Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
    Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.
     */

    let firstNumber = null;
    let secondNumber = null;

   do{
       firstNumber = + prompt("Enter first number");
       secondNumber = + prompt("Enter second number");
   } while (!(Number.isInteger(firstNumber) && Number.isInteger(secondNumber)));

   if (firstNumber > secondNumber){
       for (let i = secondNumber; secondNumber <= firstNumber; secondNumber++){
           alert(secondNumber);
       }
   } else{
       for (let i = firstNumber; firstNumber <= secondNumber; firstNumber++){
           alert(firstNumber);
       }
   }
   alert("next task --------------------------------------------------------------");


   /*
    2. Напишіть програму, яка запитує в користувача число та перевіряє,
    чи воно є парним числом. Якщо введене значення не є парним числом,1
    то запитуйте число доки користувач не введе правильне значення.
   */

    let taskTwoNumber;
    do {
        taskTwoNumber = +prompt("Напиши будь ласка парне число")
    } while (!(Number.isInteger(taskTwoNumber)) || (taskTwoNumber % 2 !== 0));

